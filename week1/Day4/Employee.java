import java.util.*;
class Employee
{
	private int empid;
	private String empname;
	protected Scanner sc;//I want to inherit in the child class.
	public Employee()
	{
		sc=new Scanner(System.in);		
	}
	public void accept()
	{
		System.out.println("Enter employee id ");
		empid=sc.nextInt();
		System.out.println("Enter Employee name");
		empname=sc.next();			
	}
	public void display()
	{
		System.out.println("Employee id "+empid);
		System.out.println("Employee name "+empname);
	}
}