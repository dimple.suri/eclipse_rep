import java.util.*;
public class EmployeeG
{
	/*
		Rules for getters and setters
			1)All the properties should start with small letters
			2) the data type of setters parameters and getters return types
			should be same
			3) all the setter method will start with setFollowed by first capital
			letter of property.
	*/
	private int empid;
	private String name;
	public void setEmpid(int empid)
	{
	this.empid=empid;
	}
	public int getEmpid()
	{
		return empid;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public String getName()
	{
		return name;
	}
}