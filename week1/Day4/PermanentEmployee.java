class PermanentEmployee extends Employee
{
	private double salary;
	public void accept()
	{
		super.accept();//It will call the accept method of super 
		//class first
		System.out.println("Enter Salary");
		salary=sc.nextDouble();
	}
	public void display()
	{
		super.display();
		System.out.println("Salary is "+salary);
	}
	public static void main(String x[])
	{
		System.out.println("calling details for super class");
		Employee emp=new Employee();
		emp.accept();
		emp.display();
		System.out.println("Calling perm employee details");
		PermanentEmployee pemp=new PermanentEmployee();
		pemp.accept();
		pemp.display();
	}
}