class Base
{
	public Base()
	{
		System.out.println("Default constructor of super class");
	}
	public Base(int x)
	{
			System.out.println("Param is "+x);
	}
}
class child extends Base
{
	public child()
	{
		super(20);//it is going to call the super class constructor which
		//is taking parameter
		System.out.println("Default constructor of child class");
	}
	public static void main(String args[])
	{
	child c1=new child();//whenever u create object of child class, it automatically
	//inherit the default constructor of super class.
	}
}