class MultiArray {
    public static void main(String[] args) {

        int[][] a = {
            {1, -2, 3}, 
            {-4, -5, 6, 9}, 
            {7}, 
        };
      
        for (int i = 0; i < a.length; ++i) {//it is calculating total number of rows
            for(int j = 0; j < a[i].length; ++j) {//it is calculating total number of columns in particular row
                System.out.print(a[i][j]+" ");
            }
			            System.out.println();

        }
    }
}