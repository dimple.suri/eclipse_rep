public class StrDemo
{
	public static void main(String arg[])
	{
		String str1=new String("HelloWorld");
		String str2=new String("HelloWorld");
		//Equals method will check the value even if address is not equal
		if(str1.equals(str2))
		{
			System.out.println("Equal strings");
		}
	}
}