import java.util.*;
public class InputDemo
{
	private Scanner sc;
	String name;
	double salary;
	int age;
	public void accept()
	{
		sc=new Scanner(System.in);
		System.out.println("Enter your name ");
		name=sc.next();//going to accept value as String
		System.out.println("Enter your salary");
		salary=sc.nextDouble();
		System.out.println("Enter Age");
		age=sc.nextInt();
		
		
	}
	public void display()
	{
		System.out.println("Name is "+name);
		System.out.println("Age is "+age);
		System.out.println("Salary is "+salary);
		
	}
	
	public static void main(String args[])
	{
	InputDemo inputobj;//Reference variable inputobj of InputDemo type
	inputobj=new InputDemo();//it is going to allocate memory to object.
	inputobj.accept();
	inputobj.display();
	
	}
	
}
/*
	sc=new Scanner(System.in)Will initialize Scanner class with keyboard
*/