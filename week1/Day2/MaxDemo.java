//I want to accept three numbers and find out the maximum among three.
import java.util.Scanner;
public class MaxDemo
{
	private Scanner sc;
	int number1;
	int number2;
	int number3;
	public void accept()//Method name
	{
		sc=new Scanner(System.in);
		System.out.println("Enter number1");
		number1=sc.nextInt();
		System.out.println("Enter Number 2");
		number2=sc.nextInt();
		System.out.println("Enter number 3");
		number3=sc.nextInt();
		
	}
	public void checkMax()//Method name
	{
		if(number1>number2&&number1>number3)
		{
			System.out.println("Number 1 is maximum number");
			
		}
		else if(number2>number1&&number2>number3)
		{
			System.out.println("Number 2 is maximum number");
		}
		else
		{
			System.out.println("Number 3 is maximum number");
		}
	}
	public static void main(String args[])
	{
		
		MaxDemo max1=new MaxDemo();//it allocates memory to object max1
		max1.accept();
		max1.checkMax();
	}
}