public class StringDemo
{
	public static void main(String arg[])
	{
		String s1="Hello";
		String s3="Hello";
		String s2=new String("Hello");
		/*Whenever u write s2=new String() it creates string in different memory
		allocation so whenever u use ==it is going to check address first then it
		will check the contents if address is not equal then it will not check contents*/
		if(s1==s2)
		{
			System.out.println("They are equals");
		}
		if(s1==s3)
		{
				System.out.println("Equal because of same pool");
		}
		else
		{
			System.out.println("They are not equal");
		}
		
	}
}