import java.util.Scanner;
public class SwitchDemo
{
	String ch;//to accept a single character
	Scanner sc;
	public void accept()
	{
	sc=new Scanner(System.in);
	System.out.println("Enter Any character");
	ch=sc.next();
	}
	public void checkChar()
	{
			switch(ch)
			{
				case "a": System.out.println("vowel a ");
						  break;
				case "e": System.out.println("Vowel e");
						  break;
				case "i":
							System.out.println("Vowel i");
							break;
				case "o":
							System.out.println("vowel o");
							break;
				case "u":
							System.out.println("vowel u");
							break;
				default:
							System.out.println("nothing in list");
			}
	}
	public static void main(String args[])
	{
	SwitchDemo s1=new SwitchDemo();
	s1.accept();
	s1.checkChar();
	}
	
}