/* in java we can have permitive data type or abstract datatype
whenever u create class it is user defined datatype.*/
public class StudentMain
{
	private Student studentarr[];//I am creating studentarr of Student type.
	public void accArr()
	{
		studentarr=new Student[4];//Initialization of Array with max 4 students
		for(int x=0;x<studentarr.length;x++)
		{
		studentarr[x]=new Student();
		//inititalizing student object in each element of arry
		
		studentarr[x].accept();//calling accept method
		System.out.println("Displaying details of students");
		studentarr[x].display();
		}
	}
	public static void main(String args[])
	{
	StudentMain smain=new StudentMain();
	smain.accArr();
	
	}
}