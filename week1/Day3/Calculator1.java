public class Calculator1
{
	int number1;
	int number2;
	public Calculator1( int number1,int number2)
	{
		this.number1=number1;//this can be used to access class level variables.
		// I am initializig parameter to class level variables.
		this.number2=number2;
		
	}
	public void display()
	{
		System.out.println("Number 1 is "+number1);
		System.out.println("Number 2 is "+number2);
	}
	public static void main(String args[])
	{
		Calculator1 c3=new Calculator1(45,56);
		c3.display();
		
	}
}