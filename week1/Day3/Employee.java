public class Employee
{
	public void givingInstruction()
	{
		System.out.println("Giving instruction to employees");
	}
	public void calculatorSalary()
	{
		System.out.println("Calculating salary of employees");
		
	}
	public void calculateContAmount()
	{
		System.out.println("Calculating contractual amount");
	}
	public static void main(String args[])
	{
	System.out.println("For manager");
		Employee managerobj=new Employee();
		managerobj.givingInstruction();
		managerobj.calculatorSalary();
		System.out.println("For employee");
		Employee regemployee=new Employee();
		regemployee.calculatorSalary();
		System.out.println("For contract employee");
		Employee contractemp=new Employee();
		contractemp.calculateContAmount();
	}
}