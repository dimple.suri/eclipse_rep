public class Calculator
{
	int number1;
	int number2;
	public Calculator()//special method which has same name as class name and it will be called //automatically as soon as u create object of class
	{
	number1=40;
	number2=30;
		System.out.println("Welcome here");
	}
	public Calculator(int x)
	{
			System.out.println("Parameter is "+x);
	}
	public void add()
	{
		System.out.println("Total is "+(number1+number2));
	}
	public void subt()
	{
		System.out.println("difference is "+(number1-number2));
	}
public static void main(String args[])
{
	Calculator c1=new Calculator();//as soon as you create object of class it will call constructor
	Calculator c2=new Calculator(20);
	c1.add();
	c1.subt();	
}
}