/*
in this example we will create two classes
Student and StudentMain
Student class i will have studentid,studentname,score
Student.java StudentMain.java
*/
import java.util.*;
public class Student{
	private int studentid;
	private String studentname;
	private int score;
	private Scanner sc;
	public void accept()
	{
		sc=new Scanner(System.in);
		System.out.println("Enter Student id");
		studentid=sc.nextInt();
		System.out.println("Enter Student name");
		studentname=sc.next();
		System.out.println("Enter Student Score");
		score=sc.nextInt();
	}
	public void display()
	{
		System.out.println("Student id is "+studentid);
		System.out.println("Studentname is "+studentname);
		System.out.println("Score is "+score);
	}
}
